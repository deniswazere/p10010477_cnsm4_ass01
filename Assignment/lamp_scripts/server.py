#!/usr/bin/env python

import socket       #importing packages
import ntplib
from time import ctime

host = ''       #left blank means localhost
port = 3456     #port 3456 which is mapped to 1234 on host
backlog = 5     #this keeps a backlog of 5 clients connected
size = 2048     #buffer size given in spec

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   #creating socket

with open('/home/vagrant/lamp_scripts/textfile/messagestore1.txt', 'a') as output_to_write: #file written
    s.bind((host,port))     #bind
    s.listen(backlog)       #listen for clients
    while 1:
        
        c = ntplib.NTPClient()      #setting NTP client up
        response = c.request('pool.ntp.org')
        client, address = s.accept()    #accept client
        data = client.recv(size)        #receive message
        if not data: break              #break if no message
        print ('Received data: '+ data +' from '+ address[0]+' at '+ctime(response.tx_time))    #append
        client.send(data)  # echo
        output_to_write.write('\nReceived '+ data +'\n')        #writing to file
        output_to_write.write('From '+ address[0]+'\n')
        output_to_write.write('At '+ ctime(response.tx_time)+'\n')
    
    client.close()      #close connection to client


