#!/usr/bin/env python

import socket

host = 'localhost'  #localhost
port = 1234         #port 1234 which is mapped to 3456 on ubuntu
size = 2048         #size given in spec
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       #create socket
s.connect((host,port))                                      #connect to host
s.send('This is my message, please accept and append it')   #string being sent
data = s.recv(size)
s.close()               #close connection
print 'Received:', data     #prints data that was sent, if it didn't reach the server will be blank on terminal