# Readme file for assignment01
## Denis McDermott
## P10010477
## CNSM4
### This repository contains a vagrant file, a bootstrap.sh file and 2 python scripts, server.py and client.py and also a messagestore1.txt file. 
### Once vagrant up has been run and the VM is provisioned you can run the server script from the VM, it is located in /home/vagrant/lamp_scripts/server.py.

### The vagrantfile contains the path to the shared folder which will have to be edited on your machine when running the code.

### The client.py file can then be run on the host machine. It will send a message which the server will receive and print back to the user, along with the IP of the client and the time the message was received.  

### This output is then printed to a textfile called messagestore1.txt, which is located in /home/vagrant/lamp_scripts/textfile/messagestore1.txt