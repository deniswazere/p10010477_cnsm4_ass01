#!/bin/bash
# Update and upgrade system
sudo aptitude -y update
sudo aptitude -y upgrade
sudo aptitude -y install python-dev
sudo apt-get -y install python-pip
sudo pip install ntplib
sudo apt-get install mysql-server-5.5
sudo aptitude -y install mysql-server mysql-client
debconf-set-selections <<< 'mysql-server mysql-server/root_password password Password'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password Password'
sudo aptitude -y install apache2
sudo /etc/init.d/apache2 restart
sudo aptitude -y install php5 libapache2-mod-php5